/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package datareaderconsole;
import java.io.*;
import java.util.*;

/**
 *
 * @author christian
 */
public class DataReaderConsole {
    public static String readLine(){
        String answer="";
        Console console=null;
        try{console=System.console();}catch(Exception ex){}
        if(console!=null){
            try{answer=console.readLine();}catch(Exception ex){}
        }
        return answer;
    }
    public static String readLine(String msg){
        String answer="";
        Console console=null;
        try{console=System.console();}catch(Exception ex){}
        if(console!=null){
            try{answer=console.readLine(msg);}catch(Exception ex){}
        }
        return answer;
    }
    public static String readPassword(){
        char [] answer={0};
        Console console=null;
        try{console=System.console();}catch(Exception ex){}
        if(console!=null){
            try{answer=console.readPassword();}catch(Exception ex){}
        }        
        return new String(answer);
    }
    public static String readPassword(String msg){
        char [] answer={0};
        Console console=null;
        try{console=System.console();}catch(Exception ex){}
        if(console!=null){
            try{answer=console.readPassword(msg);}catch(Exception ex){}
        }
        return new String(answer);
    }
}
