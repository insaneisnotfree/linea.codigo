/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jconversortemperatura;

/**
 *
 * @author christian
 */
public class JConversorTemperatura {
    
    private double FarenheitDegrees;
    private double CentigradesDegrees;
    
    public void setFarenheit(double c){
        this.FarenheitDegrees=c;
        this.CentigradesDegrees=farenheitToCentigrades(c);
    }
    public void setCentigrades(double c){
        this.CentigradesDegrees=c;
        this.FarenheitDegrees=centigradesToFarenheit(c);
    }
    public double getFarenheit(){
        return this.FarenheitDegrees;
    }
    public double getCentigrades(){
        return this.CentigradesDegrees;
    }
    /*
     Conversion de grados centigrados a farenheit
     */
    public static double centigradesToFarenheit(double degrees){
        double result=degrees;
        result=result*2-result/5;
        result=result+32;
        return result;
    }
    /*
     Conversion de grados celsius a centigrados
     */
    public static double farenheitToCentigrades(double degrees){
        double result=0;
        result=degrees-32;
        result*=5;
        result/=9;
        return result;
    }
}
