/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jmethod;
import java.util.*;

/**
 *
 * @author christian
 */
public class JMethod<T> {
    public T computeMinimum(List<T> data){
        T result=null;
        result=data.get(0);
        for(T d:data){
            if(d.toString().compareTo(result.toString())<0){
                result=d;
            }
        }
        return result;
    }
    public T computeMaximum(List<T> data){
        T result=null;
        result=data.get(0);
        for(T d:data){
            if(d.toString().compareTo(result.toString())>0){
                result=d;
            }
        }
        return result;
    }
}
