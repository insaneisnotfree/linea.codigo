/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sumanumeros;

/**
 *
 * @author xhrist14n
 */
/**
 * @file SumaNumeros.java
 * @version 1.0
 * @author Christian Portilla Pauca (http://lineadecodigo.com)
 * @date   14.Enero.2012
 * @url    http://lineadecodigo.com/
 * @description Programa que nos permite sumar numeros
 */

public class SumaNumeros {
    /**
     metodo de suma de numeros
     * Salida de tipo int (entero)
     * parametros de ingreso int,int (entero,entero)
     **/
    public static int sumarNumeros(int numero1, int numero2) {
        return numero1 + numero2;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Programa de Suma de numeros iniciando");
        //iniciamos sumando 1
        int sumando1=4234;
        System.out.println("Sumando 1: "+sumando1);
        //iniciamos sumando 2
        int sumando2=64782;        
        System.out.println("Sumando 2: "+sumando1);
        // obtenemos el resultado de la suma de los dos sumandos
        int resultado= sumarNumeros(sumando1, sumando2);
        System.out.println("Resultado: "+resultado);
        //fin de ejecucion
        System.out.println("Programa de Suma de numeros finalizando");
    }
}
