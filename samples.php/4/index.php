<? 
$variable1="hola mundo"; //cadenas largas
$variable2='hola mundo'; //cadenas cortas
$variable3="6646564"; //numero en cadenas
$variable4=34645; //numero entero simple
$variable5=57356.5645; //numero decimal simple
$variable6=null; //numero decimal simple

echo $variable1."<br>";
echo $variable2."<br>";
echo $variable3."<br>";
echo $variable4."<br>";
echo $variable5."<br>";
echo $variable6."<br>";