-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-08-2013 a las 05:53:14
-- Versión del servidor: 5.5.16
-- Versión de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `dbusuarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(200) NOT NULL,
  `clave` varchar(200) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellidos` varchar(200) DEFAULT NULL,
  `telefono` int(20) DEFAULT NULL,
  `celular` int(20) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `clave`, `nombre`, `apellidos`, `telefono`, `celular`, `descripcion`) VALUES
(7, 'usuario', 'usuario', 'alguien', 'alguien', 6784626, 749836573, 'lago mas'),
(11, 'dos', 'dos', 'dos', 'dos', 472365, 4756346, 'jfhuwhu'),
(12, 'tres', 'tres', 'tres', 'tres', 68547865, 2147483647, 'algo mas por hacer'),
(13, 'juan', 'juan', 'juan', 'portilla', 2147483647, 2147483647, 'este usuariio es uno mas'),
(14, 'juaneco', 'juaneco', 'juaneco', 'sapo', 786235764, 2147483647, 'jfljlifjjierirehoq'),
(15, 'juaneco', 'juaneco', 'juaneco', 'sapoo', 890745723, 875897324, 'lkdjfhherhjgegh  eregerqtguieyitq  wyqeyrgyu'),
(16, 'three', 'thre', '', '', 0, 0, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
