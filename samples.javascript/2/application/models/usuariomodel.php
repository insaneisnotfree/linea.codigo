<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
-------------------------------------------
Archivo creado por Christian Portilla Pauca
Mail: xhrist14n@gmail.com
-------------------------------------------
Framework Extjs 4.2
-------------------------------------------
Framework Codeigniter
-------------------------------------------
*/
class usuarioModel extends CI_Model {
	public $table;
	public function  __construct(){
		parent::__construct();
		$this->table="usuario";
	}

	public function search($nombre,$apellidos){		
		$this->db->like("nombre",$nombre);
		$this->db->like("apellidos",$apellidos);
		$data=$this->db->get($this->table);
		$all=array();
		if($data->num_rows()>0){
			foreach($data->result() as $row){
				$all[]=array(
					"id"=>$row->id,
					"usuario"=>$row->usuario,
					"clave"=>$row->clave,
					"nombre"=>$row->nombre,
					"apellidos"=>$row->apellidos,
					"telefono"=>$row->telefono,
					"celular"=>$row->celular,
					"descripcion"=>$row->descripcion
				);	
			}
			return $all;
		}else{
			return FALSE;
		}
	}
	public function create($data){
		$this->db->insert($this->table,$data);
		return $this->db->affected_rows();
	}	
	public function update($id,$data){
		$this->db->where("id",$id);
		$this->db->update($this->table,$data);
		return $this->db->affected_rows();
	}	
	public function delete($id){
		$this->db->where("id",$id);
		$this->db->delete($this->table);
		return $this->db->affected_rows();
	}	
	public function getId($id){
		$this->db->where("id",$id);
		$data=$this->db->get($this->table);
		$all=array();
		if($data->num_rows()>0){
			foreach($data->result() as $row){
				$all=array(
					"id"=>$row->id,
					"usuario"=>$row->usuario,
					"clave"=>$row->clave,
					"nombre"=>$row->nombre,
					"apellidos"=>$row->apellidos,
					"telefono"=>$row->telefono,
					"celular"=>$row->celular,
					"descripcion"=>$row->descripcion
				);	
			}
			return $all;
		}else{
			return FALSE;
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/models/notemodel.php */