<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
-------------------------------------------
Archivo creado por Christian Portilla Pauca
Mail: xhrist14n@gmail.com
-------------------------------------------
Framework Extjs 4.2
-------------------------------------------
Framework Codeigniter
-------------------------------------------
*/
class Welcome extends CI_Controller {

	public function  __construct(){
		parent::__construct();		
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */