<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
-------------------------------------------
Archivo creado por Christian Portilla Pauca
Mail: xhrist14n@gmail.com
-------------------------------------------
Framework Extjs 4.2
-------------------------------------------
Framework Codeigniter
-------------------------------------------
*/
class Usuario extends CI_Controller {

	public function  __construct(){
		parent::__construct();
		$this->load->model("usuarioModel");
		$this->load->library('session');
	}
	public function search(){
		$nombre=$this->input->post('nombre');
		$apellidos=$this->input->post('apellidos');
		$this->session->set_userdata('nombre',$nombre);
		$this->session->set_userdata('apellidos',$apellidos);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('success' => 'true')));
	}
	public function select(){
		$id=$this->input->post('id');
		$this->session->set_userdata('id',$id);
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('success' => 'true')));
	}	
	public function view(){
		$nombre=$this->session->userdata('nombre');
		$apellidos=$this->session->userdata('apellidos');
		$data=$this->usuarioModel->search($nombre,$apellidos);		
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('totalCount' => count($data),'items'=>$data)));
	}
	public function delete(){
		$id=$this->session->userdata('id');
		$data=$this->usuarioModel->delete($id);
		if($data!=FALSE){
			$data=array('success' => 'true');
		}else{
			$data=array("success"=>"false");
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}
	public function save(){
		$data=array(				
				"usuario"=>trim($this->input->post("usuario")),
				"clave"=>trim($this->input->post("clave")),
				"nombre"=>trim($this->input->post("nombre")),
				"apellidos"=>trim($this->input->post("apellidos")),
				"telefono"=>trim($this->input->post("telefono")),
				"celular"=>trim($this->input->post("celular")),
				"descripcion"=>trim($this->input->post("descripcion"))
			);
		$id=0;
		try{$id=$this->input->post("id");}catch(Exception $ex){$id=0;}
		if($id==0){
			$data=$this->usuarioModel->create($data);
		}else{
			$data=$this->usuarioModel->update($id,$data);
		}		
		if($data>0){
			$data=array('success' => 'true');
		}else{
			$data=array('success' => 'false');
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */