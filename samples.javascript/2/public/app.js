/*
-------------------------------------------
Archivo creado por Christian Portilla Pauca
Mail: xhrist14n@gmail.com
-------------------------------------------
Framework Extjs 4.2
-------------------------------------------
Framework Codeigniter
-------------------------------------------
*/
//@require @packageOverrides
Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    views: [
        'GrillaViewport',
        'UsuarioWindow'
    ],
    autoCreateViewport: true,
    name: 'MyApp',
	appFolder:'/usuarios/public/app'
});
