/*
-------------------------------------------
Archivo creado por Christian Portilla Pauca
Mail: xhrist14n@gmail.com
-------------------------------------------
Framework Extjs 4.2
-------------------------------------------
Framework Codeigniter
-------------------------------------------
*/

Ext.define('MyApp.view.GrillaViewport', {
    extend: 'Ext.container.Viewport',

    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    region: 'north',
                    frame: true,
                    height: 120,
                    layout: {
                        type: 'border'
                    },
                    title: 'Buscador',
                    items: [
                        {
                            xtype: 'form',
                            region: 'center',
                            buttons: [
                                {
                                    text: 'Buscar',
									handler:function(){
										var form=this.up('form').getForm();
										if(form.isValid()){
											form.submit(
												{
													method: 'POST',
													url:'./usuario/search',
													waitTitle: "Por favor espere",
													waitMsg: 'Enviando datos  ...',
													success: function(form, action) {
														var msg=Ext.Msg.show(
															{
																title:'Exito',
																msg:'Iniciando busqueda',
																buttons:Ext.Msg.OK,
																fn:function(key){
																	form.reset();
																	var grid=me.down("gridpanel");																	
																	grid.getStore().load();
																	grid.getView().refresh();
																	me.show();
																	return;
																},
																icon:Ext.MessageBox.INFO
															}
														);
														setTimeout(
																function(){
																	msg.hide();
																	form.reset();
																	var grid=me.down("gridpanel");
																	grid.getStore().load();
																	grid.getView().refresh();
																	me.show();
																	return;
																},
																300
															);
														return;
													},
													failure: function(form, action) {
														Ext.Msg.alert('Fallo', 'Hay un error en el envio de datos');
														return;
													}
												}
											);
										}else{
											Ext.Msg.alert('Fallo', 'Falta llenar datos en el formulario');
										}   
										return;										
									}
                                },
								{
                                    text: 'Ver todo',
									handler:function(){
										var form=this.up('form').getForm();
										form.reset();
										if(form.isValid()){
											form.submit(
												{
													method: 'POST',
													url:'./usuario/search',
													waitTitle: "Por favor espere",
													waitMsg: 'Enviando datos  ...',
													success: function(form, action) {
														var msg=Ext.Msg.show(
															{
																title:'Exito',
																msg:'Iniciando busqueda',
																buttons:Ext.Msg.OK,
																fn:function(key){
																	form.reset();
																	var grid=me.down("gridpanel");																	
																	grid.getStore().load();
																	grid.getView().refresh();
																	me.show();
																	return;
																},
																icon:Ext.MessageBox.INFO
															}
														);
														setTimeout(
																function(){
																	msg.hide();
																	form.reset();
																	var grid=me.down("gridpanel");
																	grid.getStore().load();
																	grid.getView().refresh();
																	me.show();
																	return;
																},
																300
															);
														return;
													},
													failure: function(form, action) {
														Ext.Msg.alert('Fallo', 'Hay un error en el envio de datos');
														return;
													}
												}
											);
										}else{
											Ext.Msg.alert('Fallo', 'Falta llenar datos en el formulario');
										}   
										return;										
									}
                                },
                                {
                                    text: 'Nuevo',
									handler:function(){
										var usuario=new MyApp.view.UsuarioWindow();
										usuario.show();
									}
                                },
                                {
                                    text: 'Borrar',
									handler:function(){
										Ext.Ajax.request(
										{
											url:'usuario/delete',
											method:'post',
											params:
												{
													
												}
											,success:function(response,opts){
												var responseData=null;
												try{responseData=response['responseText'];}catch(ex){responseData='';}
												var data=null;
												try{data=Ext.decode(responseData);}catch(ex){}
												if(data!=null){
													Ext.Msg.alert("Mensaje","Datos borrados");
													me.down("gridpanel").getStore().load();
													me.down("gridpanel").getView().refresh();
												}else{ 
													Ext.Msg.alert("Error","Hubo un error en el proceso");
												}
												return;
											}
										}
									);
									}
                                }
                            ],
                            frame: true,
                            layout: {
                                type: 'column'
                            },
                            bodyPadding: 10,
                            title: '',
                            items: [
                                {
                                    xtype: 'textfield',
                                    columnWidth: 0.5,
                                    padding: 5,
                                    fieldLabel: 'Nombre',
                                    name: 'nombre'
                                },
                                {
                                    xtype: 'textfield',
                                    columnWidth: 0.5,
                                    padding: 5,
                                    fieldLabel: 'Apellidos',
                                    name: 'apellidos'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'gridpanel',
                    region: 'center',
                    title: 'Usuarios',
					id:'gridpanel',
                    columns: [
						{
                            xtype: 'gridcolumn',
							hidden:true,
                            dataIndex: 'id',
                            text: 'id',							
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'usuario',
                            text: 'Usuario',							
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'nombre',
                            text: 'Nombre',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'apellidos',
                            text: 'Apellidos',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'telefono',
                            text: 'Telefono',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'celular',
                            text: 'Celular',
                            flex: 1
                        },
                        {
                            xtype: 'gridcolumn',
                            dataIndex: 'descripcion',
                            text: 'Descripcion',
                            flex: 2
                        }
                    ],
                    listeners:
                    {
                        'cellclick':function(grid,td,cellIndex,record,tr,rowIndex,event,object){
                            var id=record.get('id');
                            Ext.Ajax.request(
                                {
                                    url:'usuario/select',
                                    method:'post',
                                    params:
                                        {
                                            'id':id
                                        }
                                    ,success:function(response,opts){
                                        var responseData=null;
                                        try{responseData=response['responseText'];}catch(ex){responseData='';}
                                        var data=null;
                                        try{data=Ext.decode(responseData);}catch(ex){}
                                        if(data!=null){

                                        }else{ 
                                            Ext.Msg.alert("Error","Hubo un error en el proceso");
                                        }
                                        return;
                                    }
                                }
                            );
                            return;
                        }
                    },
                    store: Ext.create(
                            'Ext.data.Store', 
                            {
                                remoteSort: true,
                                pageSize: 200,
                                fields: [
                                    'id',
                                    'usuario',
                                    'nombre',
                                    'apellidos',
                                    'telefono',
                                    'celular',
									'descripcion'
                                ],
                                proxy: {
                                    type: 'ajax',
                                    url: '/usuarios/usuario/view',
                                    reader: {
                                        root: 'items',
                                        totalProperty: 'totalCount'
                                    }
                                },
                                autoLoad: false
                            }
                        )
                }
            ]
        });

        me.callParent(arguments);
    },
    listeners:{
        afterrender:function(cmp){
            var grid=this.down('gridpanel');
            grid.getStore().load();
			grid.getView().refresh();
            return;
        },
		activate:function(cmp){            
			var grid=this.down('gridpanel');
			grid.getStore().load();
			grid.getView().refresh();
			return;
		}		
    }
	

});