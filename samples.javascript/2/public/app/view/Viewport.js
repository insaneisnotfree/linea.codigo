/*
-------------------------------------------
Archivo creado por Christian Portilla Pauca
Mail: xhrist14n@gmail.com
-------------------------------------------
Framework Extjs 4.2
-------------------------------------------
Framework Codeigniter
-------------------------------------------
*/
Ext.define('MyApp.view.Viewport', {
    extend: 'MyApp.view.GrillaViewport',
    renderTo: Ext.getBody()
});