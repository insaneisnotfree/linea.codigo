Ext.define('MyApp.view.LayoutBorderViewport', {
    extend: 'Ext.container.Viewport',

    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'panel',
                    region: 'north',
                    height: 128,
                    itemId: 'menu',
                    title: 'Admin',
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            height: 100,
                            itemId: 'toolbar',
                            items: [
                                {
                                    xtype: 'button',
                                    handler: function(button, event) {
                                        Ext.Msg.show( 
                                        {
                                            title:'Mensaje',
                                            msg: 'Menu 1',
                                            buttons: Ext.MessageBox.OK,
                                            fn: function(){

                                                return;
                                            },
                                            icon: Ext.MessageBox.QUESTION
                                        }
                                        );
                                        return;

                                    },
                                    height: 90,
                                    width: 120,
                                    text: 'Menu 1'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, event) {
                                        Ext.Msg.show( 
                                        {
                                            title:'Mensaje',
                                            msg: 'Menu 2',
                                            buttons: Ext.MessageBox.OK,
                                            fn: function(){

                                                return;
                                            },
                                            icon: Ext.MessageBox.ERROR
                                        }
                                        );
                                        return;
                                    },
                                    height: 90,
                                    width: 120,
                                    text: 'Menu 2'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, event) {
                                        Ext.Msg.show( 
                                        {
                                            title:'Mensaje',
                                            msg: 'Menu 3',
                                            buttons: Ext.MessageBox.OK,
                                            fn: function(){

                                                return;
                                            },
                                            icon: Ext.MessageBox.WARNING
                                        }
                                        );
                                        return;
                                    },
                                    height: 90,
                                    width: 120,
                                    text: 'Menu 3'
                                },
                                {
                                    xtype: 'button',
                                    handler: function(button, event) {
                                        Ext.Msg.show( 
                                        {
                                            title:'Mensaje',
                                            msg: 'Menu 4',
                                            buttons: Ext.MessageBox.OK,
                                            fn: function(){

                                                return;
                                            },
                                            icon: Ext.MessageBox.INFO
                                        }
                                        );
                                        return;
                                    },
                                    height: 90,
                                    width: 120,
                                    text: 'Menu 4'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    region: 'west',
                    frame: true,
                    itemId: 'opciones',
                    width: 180,
                    collapsible: true,
                    title: 'Opciones'
                },
                {
                    xtype: 'panel',
                    region: 'east',
                    frame: true,
                    itemId: 'informacion',
                    width: 180,
                    collapsible: true,
                    title: 'Informacion'
                },
                {
                    xtype: 'panel',
                    region: 'center',
                    frame: true,
                    itemId: 'central',
                    title: ''
                }
            ]
        });

        me.callParent(arguments);
    }

});