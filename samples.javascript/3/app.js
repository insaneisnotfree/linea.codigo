//@require @packageOverrides
Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    views: [
        'LayoutBorderViewport'
    ],
    autoCreateViewport: true,
    name: 'MyApp'
});
